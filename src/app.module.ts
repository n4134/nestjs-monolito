import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UserModule } from './user/user.module';
import { AuthModule } from './auth/auth.module';

@Module({
  imports: [
    // ConfigModule.forRoot({
    //   envFilePath: ['.env.development'],
    //   isGlobal: true,
    // }),
    MongooseModule.forRoot('mongodb://localhost:27017/superFlights', {
      // useCreateIndex: true,
      // useFindModify: false,
    }),
    UserModule,
    AuthModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
